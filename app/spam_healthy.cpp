#include <sys/time.h>
#include <random>
#include <thread>
#include <signal.h>
#include <cstring>
#include "common/crypto/kerl/kerl.h"
#include "cclient/api/core/core_api.h"
#include "cclient/api/extended/extended_api.h"

using namespace std;

volatile bool killswitch = false;

const char * node = "";
int port = 0;
bool tls = true;
bool local_pow = true;

const char * message = "herrkpunkt healthy spam. CHRYSALIS HERE WE GO!";
const char * address = "HEALTHY99999999999999999999999999999999999999999999999999999999999999999999999999";
const char * tag = "HERRKPUNKT9HEALTHY9SPAM9999";

static char const *letsencrypt_root =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw\r\n"
    "TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\r\n"
    "cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4\r\n"
    "WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu\r\n"
    "ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY\r\n"
    "MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc\r\n"
    "h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+\r\n"
    "0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U\r\n"
    "A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW\r\n"
    "T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH\r\n"
    "B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC\r\n"
    "B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv\r\n"
    "KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn\r\n"
    "OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn\r\n"
    "jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw\r\n"
    "qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI\r\n"
    "rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV\r\n"
    "HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq\r\n"
    "hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL\r\n"
    "ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ\r\n"
    "3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK\r\n"
    "NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5\r\n"
    "ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur\r\n"
    "TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC\r\n"
    "jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc\r\n"
    "oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq\r\n"
    "4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA\r\n"
    "mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d\r\n"
    "emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=\r\n"
    "-----END CERTIFICATE-----\r\n";

void signal_handler(int s) {
  if (s == 2) {
    killswitch = true;
  }
}

void usage(){
  printf("Usage: spam_healty <NODE URL (without scheme)> <NODE PORT> <TLS (\"true\" or different)> <NUMBER OF THREADS>\n");
}

void workerThread(){
  iota_client_service_t *m_client_service;
  if(tls) m_client_service = iota_client_core_init(node, port, letsencrypt_root);
  else m_client_service = iota_client_core_init(node, port, NULL);
  while(!killswitch){
    retcode_t ret_code = RC_OK;
    uint32_t depth = 1;
    uint8_t mwm = 10;
    uint8_t security = 2;
    bundle_transactions_t *bundle = NULL;
    bundle_transactions_new(&bundle);
    transfer_array_t *transfers = transfer_array_new();
    transfer_t tf = {};
    flex_trits_from_trytes(tf.address, NUM_TRITS_ADDRESS, (const tryte_t *)address, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
    flex_trits_from_trytes(tf.tag, NUM_TRITS_TAG, (const tryte_t *)tag, NUM_TRYTES_TAG, NUM_TRYTES_TAG);
    transfer_message_set_string(&tf, message);
    transfer_array_add(transfers, &tf);
    ret_code = iota_client_send_transfer(m_client_service, NULL, security, depth, mwm, true, transfers, NULL, NULL, NULL, bundle);
    if (ret_code != RC_OK) printf("Error sending TX");
    bundle_transactions_free(&bundle);
    transfer_message_free(&tf);
    transfer_array_free(transfers);
  }
  iota_client_core_destroy(&m_client_service);
  return;
}

int main(int argc, const char *argv[]) {
#ifdef _WIN32
  signal(SIGINT, signal_handler);
#else
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
#endif
  if(argc != 5){
    usage();
    return -1;
  }
  node = argv[1];
  port = atoi(argv[2]);
  if(strcmp(argv[3], "true")) tls = false;
  int nofthreads = atoi(argv[4]);
  printf("Starting Healthy Spammer!\n");
  printf("Node is: %s\n", node);
  printf("Port is: %d\n", port);
  printf("TLS is: %d\n", tls);

  thread *thread_id[nofthreads];
  for(int i=0; i<nofthreads; i++){
    thread_id[i] = new thread(workerThread);
  }
  for(unsigned int i = 0; i < nofthreads; i++){
    thread_id[i]->join();
  }
  
  printf("Exiting Healthy Spammer!\n");
  return 0;
}
