#include <sys/time.h>
#include <random>
#include <signal.h>
#include <cstring>
#include "common/crypto/kerl/kerl.h"
#include "cclient/api/core/core_api.h"
#include "cclient/api/extended/extended_api.h"
#include <thread>

volatile bool killswitch = false;

const char * node = "";
const char * ref = "";
int port = 0;
bool tls = true;
bool local_pow = true;

const char * message = "herrkpunkt blowball spam. CHRYSALIS HERE WE GO!";
const char * address = "BLOW99999999999999999999999999999999999999999999999999999999999999999999999999999";
const char * tag = "HERRKPUNKT9BLOWBALL9SPAM999";

static char const *letsencrypt_root =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw\r\n"
    "TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\r\n"
    "cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4\r\n"
    "WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu\r\n"
    "ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY\r\n"
    "MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc\r\n"
    "h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+\r\n"
    "0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U\r\n"
    "A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW\r\n"
    "T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH\r\n"
    "B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC\r\n"
    "B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv\r\n"
    "KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn\r\n"
    "OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn\r\n"
    "jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw\r\n"
    "qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI\r\n"
    "rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV\r\n"
    "HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq\r\n"
    "hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL\r\n"
    "ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ\r\n"
    "3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK\r\n"
    "NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5\r\n"
    "ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur\r\n"
    "TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC\r\n"
    "jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc\r\n"
    "oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq\r\n"
    "4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA\r\n"
    "mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d\r\n"
    "emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=\r\n"
    "-----END CERTIFICATE-----\r\n";

void signal_handler(int s) {
  if (s == 2) {
    killswitch = true;
  }
}

void usage(){
  printf("Usage: spam_blowball <NODE URL (without scheme)> <NODE PORT> <REF TX HASH (81 trytes)> <TLS (\"true\" or different)> <NUMBER OF THREADS>\n");
}

void workerThread(){
  iota_client_service_t *m_client_service;
  if(tls) m_client_service = iota_client_core_init(node, port, letsencrypt_root);
  else m_client_service = iota_client_core_init(node, port, NULL);
  while(!killswitch){
    retcode_t ret_code = RC_OK;
    bundle_transactions_t *bundle = NULL;
    bundle_transactions_new(&bundle);
    transfer_array_t *transfers = transfer_array_new();
    transfer_t tf = {};
    flex_trits_from_trytes(tf.address, NUM_TRITS_ADDRESS, (const tryte_t *)address, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
    flex_trits_from_trytes(tf.tag, NUM_TRITS_TAG, (const tryte_t *)tag, NUM_TRYTES_TAG, NUM_TRYTES_TAG);
    transfer_message_set_string(&tf, message);
    transfer_array_add(transfers, &tf);
    hash8019_array_p raw_tx = hash8019_array_new();
    iota_transaction_t* tx = NULL;
    flex_trit_t serialized_value[FLEX_TRIT_SIZE_8019];
    ret_code = iota_client_prepare_transfers(m_client_service, NULL, 2, transfers, NULL, NULL, true, 0, bundle);
    if (ret_code == RC_OK) {
      BUNDLE_FOREACH(bundle, tx) {
        transaction_serialize_on_flex_trits(tx, serialized_value);
        utarray_insert(raw_tx, serialized_value, 0);
      }
      attach_to_tangle_req_t* attach_req = NULL;
      attach_to_tangle_res_t* attach_res = NULL;
      attach_req = attach_to_tangle_req_new();
      attach_res = attach_to_tangle_res_new();
      size_t num_trits = NUM_FLEX_TRITS_FOR_TRITS(NUM_TRITS_HASH);
      flex_trit_t ref_trits[num_trits];
      flex_trits_from_trytes(ref_trits, NUM_TRITS_HASH, (const tryte_t *)ref, NUM_TRYTES_HASH, NUM_TRYTES_HASH);
      memcpy(attach_req->branch, ref_trits, FLEX_TRIT_SIZE_243);
      memcpy(attach_req->trunk, ref_trits, FLEX_TRIT_SIZE_243);
      attach_req->mwm = 10;
      hash_array_free(attach_req->trytes);
      attach_req->trytes = raw_tx;

      ret_code = iota_client_attach_to_tangle(local_pow ? NULL : m_client_service, attach_req, attach_res);
      attach_req->trytes = NULL;
      attach_to_tangle_req_free(&attach_req);

      ret_code = iota_client_store_and_broadcast( m_client_service, (store_transactions_req_t*)attach_res);
      attach_to_tangle_res_free(&attach_res);
      attach_to_tangle_req_free(&attach_req);
    }
    if (ret_code != RC_OK) {
      printf("Error sunding TX");
    }
    hash_array_free(raw_tx);
    bundle_transactions_free(&bundle);
    transfer_message_free(&tf);
    transfer_array_free(transfers);
  }
  iota_client_core_destroy(&m_client_service);
  return;
}

int main(int argc, const char *argv[]) {
#ifdef _WIN32
  signal(SIGINT, signal_handler);
#else
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
#endif
  if(argc != 6){
    usage();
    return -1;
  }
  node = argv[1];
  port = atoi(argv[2]);
  ref = argv[3];
  if(strcmp(argv[4], "true")) tls = false;
  int nofthreads = atoi(argv[5]);
  if(strlen(ref) != 81){
    printf("Reference transaction hash has invalid length!\n");
    return -1;
  }
  printf("Starting Blowball Spammer!\n");
  printf("Node is: %s\n", node);
  printf("Port is: %d\n", port);
  printf("TLS is: %d\n", tls);

  std::thread *thread_id[nofthreads];
  for(unsigned int i = 0; i < nofthreads; i++){
    thread_id[i] = new std::thread(workerThread);
  }
  for(unsigned int i = 0; i < nofthreads; i++){
    thread_id[i]->join();
  }

  printf("Exiting Blowball Spammer!\n");
  return 0;
}
