#include <sys/time.h>
#include <random>
#include <signal.h>
#include <cstring>
#include "common/crypto/kerl/kerl.h"
#include "cclient/api/core/core_api.h"
#include "cclient/api/extended/extended_api.h"
#include <inttypes.h>
#include "common/helpers/sign.h"
#include "common/trinary/tryte.h"

using namespace std;

volatile bool killswitch = false;

const char * node = "";
const char * seed = "";
int port = 0;
bool tls = true;
bool local_pow = true;

const char * message = "herrkpunkt double spend spam. CHRYSALIS HERE WE GO!";
const char * tag = "HERRKPUNKT9DOUBLESPEND9SPAM";

static char const *letsencrypt_root =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw\r\n"
    "TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\r\n"
    "cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4\r\n"
    "WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu\r\n"
    "ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY\r\n"
    "MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc\r\n"
    "h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+\r\n"
    "0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U\r\n"
    "A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW\r\n"
    "T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH\r\n"
    "B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC\r\n"
    "B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv\r\n"
    "KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn\r\n"
    "OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn\r\n"
    "jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw\r\n"
    "qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI\r\n"
    "rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV\r\n"
    "HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq\r\n"
    "hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL\r\n"
    "ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ\r\n"
    "3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK\r\n"
    "NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5\r\n"
    "ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur\r\n"
    "TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC\r\n"
    "jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc\r\n"
    "oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq\r\n"
    "4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA\r\n"
    "mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d\r\n"
    "emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=\r\n"
    "-----END CERTIFICATE-----\r\n";

void signal_handler(int s) {
  if (s == 2) {
    killswitch = true;
  }
}

void usage(){
  printf("Usage: spam_double_spend <NODE URL (without scheme)> <NODE PORT> <SEED (81 trytes)> <TLS (\"true\" or different)>\n");
}

void workerThread(){
  iota_client_service_t *m_client_service;
  if(tls) m_client_service = iota_client_core_init(node, port, letsencrypt_root);
  else m_client_service = iota_client_core_init(node, port, NULL);
  retcode_t ret = RC_OK;
  flex_trit_t seed_trits[FLEX_TRIT_SIZE_243];
  flex_trits_from_trytes(seed_trits, NUM_TRITS_ADDRESS, (const tryte_t *)seed, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
  account_data_t account = {};
  account_data_init(&account);
  ret = iota_client_get_account_data(m_client_service, seed_trits, 2, &account);
  printf("total balance: %" PRIu64 "\n", account.balance);
  printf("unused address: ");
  flex_trit_print(account.latest_address, NUM_TRITS_ADDRESS);
  printf("\n");
  size_t addr_count = hash243_queue_count(account.addresses);
  printf("address count %zu\n", addr_count);
  for (size_t i = 0; i < addr_count; i++) {
    printf("[%ld] ", i);
    flex_trit_print(hash243_queue_at(account.addresses, i), NUM_TRITS_ADDRESS);
    printf(" : %" PRIu64 "\n", account_data_get_balance(&account, i));
  }
  char* out_1 = iota_sign_address_gen_trytes(seed, addr_count, 2);
  char* out_2 = iota_sign_address_gen_trytes(seed, addr_count+1, 2);
  printf("[%d] %s\n", addr_count, out_1);
  printf("[%d] %s\n", addr_count+1, out_2);
  //account_data_clear(&account);
  //Transfer 1
  bundle_transactions_t *bundle1 = NULL;
  bundle_transactions_new(&bundle1);
  transfer_array_t *transfers1 = transfer_array_new();
  transfer_t tf1 = {};
  flex_trits_from_trytes(tf1.address, NUM_TRITS_ADDRESS, (const tryte_t *)out_1, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
  flex_trits_from_trytes(tf1.tag, NUM_TRITS_TAG, (const tryte_t *)tag, NUM_TRYTES_TAG, NUM_TRYTES_TAG);
  tf1.value = account.balance;
  transfer_message_set_string(&tf1, message);
  transfer_array_add(transfers1, &tf1);
  inputs_t input_list = {};
  for(unsigned int i = 0; i < addr_count; i++){
    input_t input_temp = {
      .balance = account_data_get_balance(&account, i), .key_index = i, .security = 2, .address = {},
    };
    tryte_t addr_out[81];
    flex_trits_to_trytes(addr_out, 81, hash243_queue_at(account.addresses, i), NUM_TRITS_ADDRESS, NUM_TRITS_ADDRESS);
    flex_trits_from_trytes(input_temp.address, NUM_TRITS_ADDRESS, addr_out, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
    inputs_append(&input_list, &input_temp);
  }
  account_data_clear(&account);
  //Transfer 2
  bundle_transactions_t *bundle2 = NULL;
  bundle_transactions_new(&bundle2);
  transfer_array_t *transfers2 = transfer_array_new();
  transfer_t tf2 = {};
  flex_trits_from_trytes(tf2.address, NUM_TRITS_ADDRESS, (const tryte_t *)out_2, NUM_TRYTES_ADDRESS, NUM_TRYTES_ADDRESS);
  flex_trits_from_trytes(tf2.tag, NUM_TRITS_TAG, (const tryte_t *)tag, NUM_TRYTES_TAG, NUM_TRYTES_TAG);
  tf2.value = account.balance;
  transfer_message_set_string(&tf2, message);
  transfer_array_add(transfers2, &tf2);

  flex_trit_t *reminder_addr = NULL;
  flex_trit_t *reference = NULL;
  
  ret = iota_client_send_transfer(m_client_service, seed_trits, 2, 1, 10, true, transfers1, reminder_addr, reference, &input_list, bundle1);
  printf("send transfer 1 %s\n", error_2_string(ret));
  if (ret == RC_OK) {
    flex_trit_t const *bundle_hash = bundle_transactions_bundle_hash(bundle1);
    printf("bundle hash: ");
    flex_trit_print(bundle_hash, NUM_TRITS_HASH);
    printf("\n");
  }
  ret = iota_client_send_transfer(m_client_service, seed_trits, 2, 1, 10, true, transfers2, reminder_addr, reference, &input_list, bundle2);
  printf("send transfer 2 %s\n", error_2_string(ret));
  if (ret == RC_OK) {
    flex_trit_t const *bundle_hash = bundle_transactions_bundle_hash(bundle2);
    printf("bundle hash: ");
    flex_trit_print(bundle_hash, NUM_TRITS_HASH);
    printf("\n");
  }
  free(out_1);
  free(out_2);
  inputs_clear(&input_list);
  bundle_transactions_free(&bundle1);
  transfer_message_free(&tf1);
  transfer_array_free(transfers1);
  bundle_transactions_free(&bundle2);
  transfer_message_free(&tf2);
  transfer_array_free(transfers2);
  iota_client_core_destroy(&m_client_service);
  return;
}

int main(int argc, const char *argv[]) {
#ifdef _WIN32
  signal(SIGINT, signal_handler);
#else
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
#endif
  if(argc != 5){
    usage();
    return -1;
  }
  node = argv[1];
  port = atoi(argv[2]);
  seed = argv[3];
  if(strcmp(argv[4], "true")) tls = false;
  if(strlen(seed) != 81){
    printf("SEED has invalid length!\n");
    return -1;
  }
  printf("Starting Double Spend Spammer!\n");
  printf("Node is: %s\n", node);
  printf("Port is: %d\n", port);
  printf("TLS is: %d\n", tls);

  workerThread();
  printf("Exiting Double Spend Spammer!\n");
  return 0;
}
